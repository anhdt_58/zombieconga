﻿using UnityEngine;
using System.Collections;

public class ZombieController : MonoBehaviour {
	
	public float moveSpeed;
	private Vector3 moveDirection;
	public float turnSpeed;

	// Use this for initialization
	void Start () {
		moveDirection = Vector3.right;
	}
	
	// Update is called once per frame
	void Update () {
		//Copy position to local variable
		Vector3 currentPosition = transform.position;
		// 2
		if( Input.GetButton("Fire1") ) {
			// 3
			Vector3 moveToward = Camera.main.ScreenToWorldPoint( Input.mousePosition );
			// 4
			moveDirection = moveToward - currentPosition;
			moveDirection.z = 0; 
			moveDirection.Normalize();
		}

		//calculate target position that is moveSpeed units away
		Vector3 target = moveDirection * moveSpeed + currentPosition;
		//Determines new location
		transform.position = Vector3.Lerp (currentPosition, target, Time.deltaTime);
		//find angle between the x-axis and moveDirection
		float targetAngle = Mathf.Atan2(moveDirection.y, moveDirection.x) * Mathf.Rad2Deg;
		transform.rotation = 
			Quaternion.Slerp( transform.rotation, 
			                 Quaternion.Euler( 0, 0, targetAngle ), 
			                 turnSpeed * Time.deltaTime );

	}
}

